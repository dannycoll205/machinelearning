import pandas as pd
import sklearn
from sklearn.utils import shuffle
from sklearn.neighbors import KNeighborsClassifier
import numpy as np
from sklearn import linear_model, preprocessing

data = pd.read_csv("car.data")

le = preprocessing.LabelEncoder()
buying = le.list(data["buying"])
maint = le.list(data["maint"])
door = le.list(data["door"])
persons = le.list(data["persons"])
lug_boot = le.list(data["lug_boot"])
safety = le.list(data["safety"])
cls = le.list(data["class"])

predict = "class"

x = list(zip(buying, maint, door, persons, lug_boot, safety))
y = list(cls)

x_train, x_test, y_train, y_test  = sklearn.model_selection.train_test_split(x, y, test_size=0.1)

